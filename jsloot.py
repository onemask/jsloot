import asyncio
import sys
import re
import os
import jsbeautifier
import logging
import time
import requests
from datetime import datetime, timedelta
from urllib3.exceptions import InsecureRequestWarning
from rich.console import Console
from rich.logging import RichHandler

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)


class Logger():
    def __init__(self, level="INFO"):
        # FORMAT = f"%(name)s\t%(message)s"
        FORMAT = "%(message)s"
        logging.basicConfig(
            level=level,
            format=FORMAT,
            datefmt="[%X]",
            handlers=[RichHandler()]
        )

    def get_logger(self, name):
        return logging.getLogger(name.upper())


class JSLoot():

	_jsfiles = []
	_success = 0
	_error = 0
	_urls = []

	def __init__(self, args):
		"""Initialize logger and downloader
		"""
		# Get time
		self.start = time.time()
		self.now = datetime.now().strftime("%Y%m%d-%H%M%S")
		self.args = args
		self.target = self.args.url
		self.scheme = self.target.split('/')[0].rstrip(':')
		self.domain = self.target.split('/')[2]

		if args.verbose:
			self._logger = Logger("DEBUG").get_logger("jsloot")
		else:
			self._logger = Logger().get_logger("jsloot")

		self._jsfiles = set()
		if args.output:
			self.output_dir = args.output
		else:
			self.output_dir = f"jsloot_{self.domain}"

		self._logger = logging.getLogger("jsloot")
		self._logger.info("INIT\t\tTarget: '%s'", self.target)

		# INIT SESSION REQUESTS OBJECT
		self._init_http_session(self.args)
		self._create_folder_structure()

	def _init_http_session(self, args):
		self.session = requests.Session()
		if args.proxy:
			self.session.proxies.update({'all':args.proxy})
		self.session.verify = False
		ua = ""
		ua += "Mozilla/5.0 (X11; Linux x86_64; rv:92.0) "
		ua += "Gecko/20100101 Firefox/92.0"

		headers = {
			"User-Agent": ua,
		}
		self.session.headers.update(headers)

	def _create_folder_structure(self):

		self._logger.info("INIT\t\tInitiating folder structure")

		# Create output dir (i.e. output/)
		if not os.path.exists(self.output_dir):
			self._logger.debug("INIT\t\tCreating folder: %s", self.output_dir)
			os.makedirs(self.output_dir)

		# Create hostname dir (e.g. output/onemask.me)
		hostname_dir = self.output_dir + "/" + self.domain
		if not os.path.exists(hostname_dir):
			self._logger.debug("INIT\t\tCreating folder: %s", hostname_dir)
			os.makedirs(hostname_dir)

		# Create ts dir (e.g output/onemask.me/20223101-011203)
		self.ts_dir = hostname_dir + "/" + self.now
		if not os.path.exists(self.ts_dir):
			self._logger.debug("INIT\t\tCreating folder: %s", self.ts_dir)
			os.makedirs(self.ts_dir)

		# Create raw_js dir
		self._raw_js_dir = self.ts_dir + "/00-raw_js_files"
		if not os.path.exists(self._raw_js_dir):
			self._logger.debug('INIT\t\tCreating "00-raw_js_files" folder')
			os.makedirs(self._raw_js_dir)

		# Create beautified_js dir
		self._beautified_js_dir = self.ts_dir + "/01-beautified_js_files"
		if not os.path.exists(self._beautified_js_dir):
			self._logger.debug(
				'INIT\t\tCreating "01-beautified_js_files" folder'
				)
			os.makedirs(self._beautified_js_dir)

	def _get_file_name(self, url):
		firstpos = url.rfind("/")
		lastpos = len(url)
		return (url[firstpos+1:lastpos])

	async def _get_files(self):
		"""Find all JS files in target page
		"""
		self._logger.info("GET-FILES\tQuerying host..")
		try:
			res = self.session.get(self.target, timeout=5)
			regex = "[0-9A-Za-z\.\-_\/\:]+\.js"
			files = re.findall(regex,res.text)
			if files:
				self._jsfiles = set(files)
				self._logger.debug(self._jsfiles)
				self._logger.info(
					f"GET-FILES\tFound {len(self._jsfiles)} unique JS files",
					)
				return True, set(files)
			else:
				self._logger.info("GET-FILES\tNo JS files found")
				await self._end()
				return False, None
		except Exception as e:
			self._logger.info("GET-FILES\tHost timeout: %s", e)
			raise SystemError("Host timeout")

	async def _is_excluded(self, elt):
		ignored_files = [
			'bootstrap.min.js',
			'bootstrap.js',
			'jquery.min.js',
			'jquery.js'
		]
		for i in ignored_files:
			return True if i in elt else False

	async def _is_in_scope(self, file):
		domain = file.split('/')[2]
		self._logger.debug(f"IS IN SCOPE\tDomain: {domain}")
		if domain == self.domain:
			self._logger.debug(
				f'IS IN SCOPE\tAdding {file} to download list'
				)
			return True
		else:
			self._logger.debug(
				f'IS IN SCOPE\tExcluding {file} from '
		      	f'download list')
			return False

	async def _filter_urls(self, files):
		"""Generate a list of URLs to download
		"""

		url_out_of_scope = 0
		url_excluded     = 0
		urls             = []

		for elt in files:
			self._logger.debug("FILTER-URL\tProcessing '%s'", elt)
			if not await self._is_excluded(elt):
				# If target is not in the domain scope
				if (elt.startswith("http") or elt.startswith("https")):
					if not await self._is_in_scope(elt):
						url_out_of_scope += 1
					else:
						urls.append(elt)
				elif elt.startswith('//'):
					urls.append("https:" + elt)
				elif elt.startswith('/'):
					urls.append(self.scheme + "://" + self.domain + elt)
				else:
					urls.append(self.target + "/" + elt)
			else:
				url_excluded += 1

		self._logger.debug('FILTER-URL\tDownload list:')
		for f in urls:
			self._logger.debug(f'FILTER-URL\t\t{f}')

		if url_out_of_scope > 0:
			self._logger.info(
				f"FILTER-URL\tIgnoring {url_out_of_scope} file(s) (not in scope)"
				)

		if url_excluded > 0:
			self._logger.info(
				f"FILTER-URL\tIgnoring {url_excluded} file(s) (exclusion list)"
				)

		return urls

	async def _download_file(self, file):
		self._logger.debug('FETCH-URL\tQuerying: [%s] ',file)
		# We also simultaneously search for a corresponding .map file here
		await self._search_map_file(file)

		# Meanwhile we still download the .js file
		res = self.session.get(file, stream=True)
		if res.status_code == 200:
			open(f'{self._raw_js_dir}/{self._get_file_name(file)}','wb') \
			.write(res.content)
			self._success += 1
			self._logger.info('FETCH-URL\tDownloading: %s',file)
		else:
			self._logger.info(
				f'FETCH-URL\tError while downloading: {file} '
				f'[{res.status_code}]'
				)
			self._error += 1

	async def _beautify_file(self, f):
		self._logger.info("JS-BEAUTIFY\tBeautifying %s", f)
		open(f"{self._beautified_js_dir}/{self._get_file_name(f)}","w") \
			.write(jsbeautifier.beautify_file(self._raw_js_dir + "/" + f))

	async def _beautify_files(self):
		"""Beautify files found in self._path
		"""
		files = [f for f in os.listdir(self._raw_js_dir) if \
			os.path.isfile(os.path.join(self._raw_js_dir, f))]
		for f in files:
			tasks = []
			tasks.append(asyncio.create_task(self._beautify_file(f)))

			await asyncio.wait(tasks)

	async def _search_map_file(self, elt):

		not_map_file = ['<!doctype','<head>','<title>']
		map_folder   = self.ts_dir + "/03-MAP_files"
		url          = elt + ".map"
		res          = self.session.get(url)

		if res.ok:
			skip = [ele for ele in not_map_file if(ele in res.text)]
			if skip:
				return
			# Create file structure
			if not os.path.exists(map_folder):
				self._logger.debug(
					f"SEARCH-MAP\tCreating folder: {map_folder}"
					)
				os.makedirs(map_folder)
			filename = self._get_file_name(url)
			try:
				with open(map_folder + "/" + filename, 'w') as f:
					f.write(res.text)
				self._logger.info(
					f'SEARCH-MAP\tGot a [200] for {filename} !'
					)
			except Exception as e:
				self._logger.error(
					f'SEARCH-MAP\tFail saving map file {filename}'
					)

	async def _end(self):
		"""End the downloader class
		"""
		output_folder = self.output_dir + "/" + self.domain + "/" + self.now
		self.end = time.time()
		if self._success > 0:
			self._logger.info(
				f"END\t\tFinished downloading and beautifying files"
				)
			self._logger.info(
				f"END\t\tFiles available in './{output_folder}'"
				)
		self._logger.info(
			f"END\t\t{self._success} succeeded, {self._error} "
			f"failed in {timedelta(seconds=(self.end - self.start))} second(s)."
			)

	async def run(self):

		ret, files = await self._get_files()
		if ret:
			urls = await self._filter_urls(files)
			if len(urls) > 0:
				self._logger.debug(
					f'RUN\t\tDownload list:')
				for f in urls:
					self._logger.debug(f'RUN\t\t\t{f}')
				tasks = []
				for file in urls:
					tasks.append(asyncio.create_task(self._download_file(file)))

				results = await asyncio.wait(tasks)
				await self._beautify_files()
				await self._end()

			else:
				self._logger.info("No URL found, exiting")
				raise SystemError("No URL found, exiting")
