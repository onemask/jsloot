# JSLOOT

## DESCRIPTION

This tools parses a website and download all JS files found then beautified them
and search for each one a corresponding `.map` file and if found, download it.

It is not a recursive crawler and the tool main goal is to find `.map` files.

Then we can use tools like `sourcemapper` to reconstruct the front-end and
search within the source files any relevant usefull leak/information.

## INSTALLATION

```bash
git clone https://gitlab.com/onemask/jsloot
cd jsloot
# Create a venv, good practice
python3 -m venv .py3 && source .py3/bin/activate
# Run the program
python3 main.py --help
```

## USAGE

```console
.py3) @work ➜ jsloot git:(master) python3 main.py --help
     _ ____  _                _
    | / ___|| |    ___   ___ | |_
 _  | \___ \| |   / _ \ / _ \| __|
| |_| |___) | |__| (_) | (_) | |_
 \___/|____/|_____\___/ \___/ \__|

       ~~Loot all the things~~

                   @0nemask - 2022


usage: main.py [-h] -u URL [-o OUTPUT] [-x PROXY] [-v]

Parse a website and download all JS files found then beautified them and search for each one a corresponding`.map` file and if found, download it.

options:
  -h, --help            show this help message and exit
  -u URL, --url URL     The target URL
  -o OUTPUT, --output OUTPUT
                        Output folder (default is the 'output' directory)
  -x PROXY, --proxy PROXY
                        Proxy URL (e.g. http://127.0.0.1:8080)
  -v, --verbose         Verbose output (debug mode)
```

## EXAMPLES

Basic usage:

![](./img/jsloot_1.png)

Example where `.map` files have been found:

![](./img/jsloot_2.png)

Resulting directory structure:

![](./img/jsloot_3.png)

## Author

- `onemask`
