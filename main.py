#!/usr/bin/env python3
#
# JSLOOT
#
# Author: onemask

import argparse
import asyncio

import jsloot


class LoggerWriter:
    def __init__(self, logger, level):
        self.logger = logger
        self.level = level

    def write(self, message):
        if message != '\n':
            self.logger.log(self.level, message)


def banner():

    banner = ""
    banner += "     _ ____  _                _\n"
    banner += "    | / ___|| |    ___   ___ | |_\n"
    banner += " _  | \___ \| |   / _ \ / _ \| __|\n"
    banner += "| |_| |___) | |__| (_) | (_) | |_\n"
    banner += " \___/|____/|_____\___/ \___/ \__|\n\n"
    banner += "       ~~Loot all the things~~\n\n"
    banner += "                   @0nemask - 2022\n\n"
    print(banner)


def parse_args():
    description = ""
    description += "Parse a website and download all JS files found then "
    description += "beautified them and search for each one a corresponding"
    description += "`.map` file and if found, download it."

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-u', '--url', help="The target URL", required=True)
    parser.add_argument(
        '-o',
        '--output',
        help="Output folder (default is the 'output' directory)",
        required=False
        )
    parser.add_argument(
        '-x',
        '--proxy',
        help="Proxy URL (e.g. http://127.0.0.1:8080)",
        required=False
        )
    parser.add_argument(
        '-v',
        '--verbose',
        help="Verbose output (debug mode)",
        action="store_true",
        required=False
        )

    return parser.parse_args()


async def run(args):
    x = jsloot.JSLoot(args)
    await x.run()


def main():
    args = parse_args()
    asyncio.run(run(args))


if __name__ == '__main__':
    banner()
    main()
